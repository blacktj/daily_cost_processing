import email
import imaplib
import os
import html2text
import time
import pandas as pd
pd.options.display.max_columns= None
import psycopg2
import boto3
import requests
import zipfile

local_dir = 'C:/Users/Travis/Downloads'
flock_url = 'https://api.flock.com/hooks/sendMessage/07829832-8b86-4772-a120-4bc115b3c9de'
con = psycopg2.connect(dbname='events', host='web-apps.cyuhuvfpttsm.us-west-2.redshift.amazonaws.com',
                       port='5439', user='travisb_rw', password='User8#Travis')

account_dict = {'tagX1249193-localweather': 'AccurateWeatherPro_Chrome_Placements',
                'tagX1249193-email': 'Chrome_Email_BroadTarget_Exchanges',
                'tagX1249193-pdfconverter': 'PDF Pro_CPM_Chrome',
                'tagX1249193-pdfconverter': 'PDF Pro_CPA+Chrome',
                'tagX1249193-tv': 'StreamTVLive_Chrome_Audiences_CPM',
                'tagX1202705-TV': 'StreamTVLive_Chrome_Audiences_CPM',
                'tagX1249193-maps': 'MFO_Chrome_BroadTargeting',
                'tagX1249193-news': 'NewsTrackr_Chrome_Exchanges'}

dim_1_dict = {'tagX1249193-localweather': '3738507',
              'tagX1249193-email': '3642673',
              'tagX1249193-pdfconverter': '3679757',
              'tagX1249193-pdfconverter': '3802242',
              'tagX1249193-tv': '3727837',
              'tagX1202705-TV': '3727837',
              'tagX1249193-maps': '3642674',
              'tagX1249193-news': '3695720'}


def get_attachment(local_dir, partner_name):
    '''
    Accesses the Mediavo Reporting Gmail Inbox, finds most recent applicable
    attachment and returns the path where CSV file is stored.
    '''
    if partner_name == 'DBM':
        gmail_folder = "[Gmail]/Starred"
    elif partner_name == 'Softonic':
        gmail_folder = 'Softonic'
    elif partner_name == 'Max_Costs':
        gmail_folder = 'MEDIANET'
    else:
        print('Error')
    #requests.post(flock_url, str({'text': 'Starting Cost Process for for' + partner_name + 'today'}))
    mail = imaplib.IMAP4_SSL('imap.gmail.com')
    mail.login('reporting@mediavo.com', 'KCRoyals2015')
    mail.select(gmail_folder)

    typ, inbox_item_list = mail.search(None, 'ALL')
    email_id = len(inbox_item_list[0].decode().split(' '))
    typ, email_data = mail.fetch(str(email_id), '(RFC822)')
    raw_email = email_data[0][1].decode("UTF-8")
    email_message = email.message_from_string(raw_email)

    for part in email_message.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue

        if gmail_folder == 'Softonic':
            filename = 'Softonic.csv'
            att_path = os.path.join(local_dir, filename)
            os.remove(att_path)
        else:
            filename = part.get_filename()
            att_path = os.path.join(local_dir, filename)

        if not os.path.isfile(att_path):
            print('Yes')
            fp = open(att_path, 'wb')
            fp.write(part.get_payload(decode=True))
            fp.close()
            print('Downloaded file:', filename)
    return att_path


def get_cost_mapping(con):
    '''
    Pulls the cost mapping from Redshift for DBM and Softonic.
    '''
    cur = con.cursor()
    cur.execute("select source, trfsrc, account, dim_1 from public.dim_cost_account_mapping where source in ('DBM')")
    res = cur.fetchall()
    colnames = [desc[0] for desc in cur.description]
    cur.close()
    df = pd.DataFrame(res, columns=colnames)
    return df


def write_final_cost(df):
    '''
    Writes final cost dataframe to Redshift.
    '''
    if input('Write DF to Redshift?').lower() == 'y':
        final_list = df.values.tolist()
        query = 'insert into public.cost_adjustments (source, mcc, account, dim_1, trfsrc, date, cost, comments) values (%s, %s, %s, %s, %s, %s, %s, %s)'
        cur = con.cursor()
        cur.executemany(query, final_list)
        con.commit()
        cur.close()
        #requests.post(flock_url, str({'text': 'DBM Cost Process finished for today'}))
    return None


def set_days(con):
    '''
    Returns max date to use as filter for the cost df for upload
    '''
    cur = con.cursor()
    query = "select max(date) from PUBLIC.cost_adjustments where source = 'DBM'"
    cur.execute(query)
    min_date = cur.fetchone()
    return min_date


def update_unmapped_trfsrc(df):
    '''
    Need to clean up -- Should get any unmapped campaigns and add them to the cost_account mapping table.
    '''
    update_list = df[(df.trfsrc.isna()) & (~df.Campaign.isna())].loc[:,['Campaign', 'Campaign ID']].drop_duplicates().values.tolist()[0]
    update_list.append('DBM')
    update_list[1] = str(update_list[1])
    update_list.append(datetime.datetime.now().strftime('%m/%d/%Y'))
    update_list.append(datetime.datetime.now().strftime('%m/%d/%Y'))
    update_list.append(datetime.datetime.now().strftime('%m/%d/%Y'))
    update_tuple = tuple(update_list)
    query = 'insert into public.dim_cost_account_mapping (account, dim_1, source, created_date, first_event_date, last_event_date) values {}'.format(update_tuple)
    cur = con.cursor()
    cur.execute(query)
    con.commit()
    cur.close()

def dbm_read_csv(path, con):
    '''
    Pulls mapping, max date (min date for filter) and CSV from storage to create final DF for upload to Redshift.
    '''
    mapping = get_cost_mapping(con)
    min_day = pd.Timestamp(set_days(con)[0])

    mapping_dict = {}
    for e, i in enumerate(mapping.trfsrc):
        mapping_dict[str(mapping['account'][e])] = i

    df_all = pd.read_csv(path)
    df_all['trfsrc'] = df_all.Campaign.map(mapping_dict)

    # update_unmapped_trfsrc(df_all)

    pivot_all = df_all.pivot_table(index=['Date', 'trfsrc'], values='Total Media Cost (Advertiser Currency)', aggfunc=sum).reset_index()
    pivot_all['account'] = pivot_all.trfsrc.map(account_dict)
    pivot_all['dim_1'] = pivot_all.trfsrc.map(dim_1_dict)
    pivot_all.columns = ['date', 'trfsrc', 'cost', 'account', 'dim_1']
    pivot_all['source'] = 'DBM'
    pivot_all['mcc'] = 'DBM'
    pivot_all['comments'] = 'from daily email-py automation'
    pivot_all['date'] = pd.to_datetime(pivot_all.date)
    df_final = pivot_all.loc[:, ['source', 'mcc', 'account', 'dim_1', 'trfsrc', 'date', 'cost', 'comments']]
    df_final = df_final[df_final.date > min_day]
    print(df_final)
    write_final_cost(df_final)

    return None

def move_softonic_to_s3(filename):
    s3 = boto3.resource('s3')
    data = open(filename, 'rb')
    s3.Bucket('new-providers').put_object(Key='Softonic.csv', Body=data)
    return None


def extract_zip_max_costs(path):
    with zipfile.ZipFile(path, 'r') as zip_ref:
        upd_path = os.path.join(local_dir)
        zip_ref.extractall(local_dir)
        filename = zip_ref.namelist()[0]
        data = open(os.path.join(local_dir, filename), 'rb')
        s3 = boto3.resource('s3')
        s3.Bucket('new-providers').put_object(Key='max_costs_update.csv', Body=data)
    return None

if __name__ == "__main__":
    dbm_read_csv(get_attachment(local_dir, 'DBM'), con)
    move_softonic_to_s3(get_attachment(local_dir, 'Softonic'))
    extract_zip_max_costs(get_attachment(local_dir, 'Max_Costs'))
