from selenium import webdriver
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import time
import pandas as pd
import requests
import psycopg2

''' Set days to run process '''
# start_date = datetime(2020,1,11)
# end_date = datetime(2020,1,11)

try:
    flock_url = 'https://api.flock.com/hooks/sendMessage/07829832-8b86-4772-a120-4bc115b3c9de'
    con = psycopg2.connect(dbname='events', host='web-apps.cyuhuvfpttsm.us-west-2.redshift.amazonaws.com', port='5439',
                           user='', password='')
    cur = con.cursor()

    ''' Pull cost account mapping to map account - TRFSRC '''
    query = "select source, trfsrc, account, dim_1 from public.dim_cost_account_mapping where source in ('RTX', 'PPCBUZZ')"
    cur.execute(query)
    data = cur.fetchall()
    df_trfsrc_mapping = pd.DataFrame(data, columns=[desc[0] for desc in cur.description])
    rtx_dict = df_trfsrc_mapping[df_trfsrc_mapping.source == 'RTX'].loc[:, ['account', 'trfsrc']].set_index('account').T.to_dict('records')[0]
    ppc_dict = df_trfsrc_mapping[df_trfsrc_mapping.source == 'PPCBUZZ'].loc[:, ['dim_1', 'trfsrc']].set_index('dim_1').T.to_dict('records')[0]
except Exception as ex:
    requests.post(flock_url, str({'text': 'DB Connection error in Cost Process: ' + ex.__str__()}))

'''PPCBUZZ DATA PULL'''
try:
    '''Set days to run process'''
    query = "select max(date)+1 from PUBLIC.cost_adjustments where source = 'PPCBUZZ'"
    cur.execute(query)
    start_date = cur.fetchall()[0][0]
    start_date = datetime(start_date.year, start_date.month, start_date.day)
    query = "select current_date-1"
    cur.execute(query)
    end_date = cur.fetchall()[0][0]
    end_date = datetime(end_date.year, end_date.month, end_date.day)

    if start_date <= end_date:
        requests.post(flock_url, str({'text': 'Starting Cost Process for PPCBUZZ for date '+start_date.__str__()+' to '+end_date.__str__()}))

        '''PPC BUZZ API request'''
        xml_dat = requests.get('https://advert.ppc.buzz/reports/getStatsByDateAndCampaign?id=2104&token=de4b7c4e75dacdc9578c4e3e5a40fb93&date_from=' + (start_date).strftime('%Y-%m-%d') + '&date_to=' + end_date.strftime('%Y-%m-%d'))
        list_ppc = []
        soup = BeautifulSoup(xml_dat.content, 'html.parser')
        for row in soup.find_all('entry'):
            detail = row.get_text(strip=True, separator='|').split('|')
            list_ppc.append(['PPCBUZZ', 'PPCBUZZ', detail[1], detail[0], float(detail[5]), ])

        '''Replace numbered campaigns to cut down, but remain flexible on non-weather campaigns

        '''
        df_ppc = pd.DataFrame(list_ppc, columns=['mcc', 'source', 'account', 'date', 'cost'])
        df_ppc['account'] = df_ppc['account'].str.replace('(.[0-9])', '')  # will remove _(number) in campaigns to consolidate but not replace
		df_ppc = df_ppc.pivot_table(index=['mcc', 'source', 'account', 'date'], values='cost', aggfunc=sum).reset_index()
        df_ppc['trfsrc'] = df_ppc['account'].map(ppc_dict, na_action='ignore')  # map trfsrc values

        if df_ppc.empty:
            print('Empty Dataframe')
            requests.post(flock_url, str({'text': 'PPCBUZZ data is not available for above mentioned date'}))
        else:
            '''clean'''
            df_ppc['dim_1'] = df_ppc['account']
            df_ppc = df_ppc.loc[:, ['mcc', 'source', 'account', 'dim_1', 'trfsrc', 'date', 'cost']]
            df_ppc['comments'] = 'from daily cost automation'
            list_final_partner = df_ppc.values.tolist()

            '''Insert PPCBUZZ data into cost adjustment table'''
            query = "insert into PUBLIC.cost_adjustments (mcc, source, account, dim_1, trfsrc, date, cost, comments) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            cur.executemany(query, list_final_partner)
            con.commit()

            '''Insert entries into log table'''
            query = "insert into public.cost_process_log (log_date, partner_name) VALUES (current_date, 'PPCBUZZ')"
            cur.execute(query)
            con.commit()
            requests.post(flock_url, str({'text': 'Completed Cost Process for PPCBUZZ'}))
            print("PPC Complete")

    else:
        requests.post(flock_url, str({'text': 'PPCBUZZ Cost Process data already exist for ' + end_date.__str__()}))
except Exception as ex:
    requests.post(flock_url, str({'text': 'Error in PPCBUZZ Cost Process: ' + ex.__str__()}))

'''RTX DATA PULL'''
try:

    '''Set days to run process'''
    query = "select max(date)+1 from PUBLIC.cost_adjustments where source = 'RTX'"
    cur.execute(query)
    start_date = cur.fetchall()[0][0]
    start_date = datetime(start_date.year, start_date.month, start_date.day)
    query = "select current_date-1"
    cur.execute(query)
    end_date = cur.fetchall()[0][0]
    end_date = datetime(end_date.year, end_date.month, end_date.day)

    if start_date <= end_date:
        requests.post(flock_url, str({'text': 'Starting Cost Process for RTX for date '+start_date.__str__()+' to '+end_date.__str__()}))

        ''' RTX cost
            - Open Selenium browser, log-in, get costs into dataframe
        '''
        link = 'https://app.rtxplatform.com/pops/campaigns?perpage=2000&page=1&order=spend__desc&filter_status=active%2Cpaused&start_date=' + str(start_date.strftime("%Y-%m-%d")) + '&end_date=' + str(start_date.strftime("%Y-%m-%d"))
        options = webdriver.chrome.options.Options()
        options.add_argument("--headless")  # Runs Chrome in headless mode.
        options.add_argument('--no-sandbox')  # Bypass OS security model
        options.add_argument('start-maximized')
        options.add_argument('disable-infobars')
        options.add_argument("--disable-extensions")
        browser = webdriver.Chrome(executable_path=r"/usr/bin/chromedriver", chrome_options=options)
        browser.get(link)

        elem_username = browser.find_element_by_xpath('//*[@id="email"]')
        elem_username.click()
        elem_username.send_keys('andrew@mediavo.com')

        elem_password = browser.find_element_by_xpath('//*[@id="password"]')
        elem_password.click()
        elem_password.send_keys('KCRoyals2015')

        elem_login = browser.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/div/div[1]/form/div[5]/div/button')
        elem_login.click()

        all_list = []

        while start_date < (end_date + timedelta(1)):
            for i in ['38621/push', 'pops']:
                link = 'https://app.rtxplatform.com/' + i + '/campaigns?perpage=2000&page=1&order=spend__desc&filter_status=active%2Cpaused&start_date=' + str(start_date.strftime('%Y-%m-%d')) + '&end_date=' + str(start_date.strftime('%Y-%m-%d'))

                browser.get(link)
                time.sleep(10)
                soup = BeautifulSoup(''.join(browser.page_source), 'html.parser')

                row_list = []
                for table in soup.find_all('tbody'):
                    for row in table.find_all('tr'):
                        detail = (row.get_text(strip=True, separator='|').split('|'))
                        if detail[0] == 'Activate':
                            detail_for_append = [detail[3], detail[13], start_date, i]
                            row_list.append(detail_for_append)
                        elif detail[0] == 'Active':
                            detail_for_append = [detail[4], detail[17], start_date, i]
                            row_list.append(detail_for_append)
                    all_list.append(row_list)
            start_date += timedelta(1)
        browser.quit()

        '''Clean RTX data'''
        df_rtx_pop = pd.DataFrame([item for sublist in all_list for item in sublist], columns=['campaign', 'cost', 'date', 'type'])
        df_rtx_pop = df_rtx_pop[df_rtx_pop.date < datetime.today()]
        df_rtx_pop = df_rtx_pop.drop_duplicates()
        df_rtx_pop.cost = df_rtx_pop.cost.str.replace('$', '')
        df_rtx_pop = df_rtx_pop[df_rtx_pop.cost != '-']
        df_rtx_pop.cost = df_rtx_pop.cost.astype(float)
        df_rtx_pop.campaign = df_rtx_pop.campaign.replace(to_replace=r' -(.*)', value='', regex=True)
        df_rtx_pop_final = df_rtx_pop.pivot_table(index=['date', 'campaign'], values='cost', aggfunc=sum).reset_index()

        '''Ready RTX data for clean list'''
        list_partner = []
        df_rtx_pop_final['source'] = 'RTX'
        df_rtx_pop_final['mcc'] = 'RTX'
        df_rtx_pop_final.rename(columns={'campaign': 'account'}, inplace=True)
        df_rtx_pop_final['dim_1'] = df_rtx_pop_final['account']
        df_rtx_pop_final = df_rtx_pop_final.iloc[:, [4, 3, 1, 5, 0, 2]]
		df_rtx_pop_final['comments'] = 'from daily cost automation'
        df_rtx_pop_final['trfsrc'] = df_rtx_pop_final['account'].map(rtx_dict, na_action='ignore')
        list_final_partner = df_rtx_pop_final.values.tolist()

        '''Insert RTX data into cost adjustment table'''
        query = "insert into PUBLIC.cost_adjustments (mcc, source, account, dim_1, date, cost, comments, trfsrc) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        cur.executemany(query, list_final_partner)
        con.commit()

        '''Insert entries into log table'''
        query = "insert into public.cost_process_log (log_date, partner_name) VALUES (current_date, 'RTX')"
        cur.execute(query)
        con.commit()
        print("RTX Complete")
        requests.post(flock_url, str({'text': 'Completed Cost Process for RTX'}))
        cur.close()
        con.close()
    else:
        requests.post(flock_url, str({'text': 'RTX Cost Process data already exist for '+end_date.__str__()}))
except Exception as ex:
    print(ex.__str__())
    requests.post(flock_url, str({'text': 'Error in RTX Cost Process: ' + ex.__str__()}))
