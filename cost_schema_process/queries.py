import psycopg2
import pandas as pd
import re

def redshift_connection(uname, pword):
    con = psycopg2.connect(dbname='events', host='web-apps.cyuhuvfpttsm.us-west-2.redshift.amazonaws.com', port='5439',
                           user=uname, password=pword)
    return con

def select_query_df(con, qry):
    cur = con.cursor()
    cur.execute(qry)
    res = cur.fetchall()
    colnames = [desc[0] for desc in cur.description]
    cur.close()
    df = pd.DataFrame(res, columns = colnames)
    return df

def select_query_table(con, table):
    cur = con.cursor()
    cur.execute("select pg_get_viewdef('public." + str(table) + "', true)")
    res = cur.fetchone()
    cur.close()
    return res

def clean_query_findall(string): 
    return re.findall('desc\)from(.*?)\.', string[0].lower().replace('\n','').replace(' ',''))

def select_query_list(con, qry):
    cur = con.cursor()
    cur.execute(qry)
    res = cur.fetchall()
    cur.close()
    return res