import psycopg2
import pandas as pd

from queries import *

def list_to_string(list):
    """Converts a list to a string."""
    x = ' '.join(list)
    return x

def parse_queries(sql_file):
    """Parses queries from a SQL file. Returns a list of queries."""
    f = open(sql_file, 'r')
    query_line = []
    for block in f.read().replace('\t', '').split('\n'):
        if block[:2] == '--':
            next
        elif block == '':
            next
        else:
            query_line.append(block)
    clean_queries = list_to_string(query_line).split('; ')
    return clean_queries

def main():
    query = 'select * from reporting_mart.tmp_schemas_for_accountid'
    con = redshift_connection(uname='travisb_rw', pword='User8#Travis')
    all_schema = select_query_df(con, query)

    clean_queries = parse_queries('dim_Schema_Tables_Account_Map_Prod.sql')

    for qry in clean_queries:
        print(qry) # replace with CronBot when ready
        cur = con.cursor()
        cur.execute(qry)
        con.commit()
        cur.close()
    return None

if __name__ == '__main__':
    main()
