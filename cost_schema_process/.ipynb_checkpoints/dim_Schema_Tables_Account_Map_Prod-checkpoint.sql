/*
	Get the tables from which to source the accountid/customerid
*/
drop table if exists reporting_mart.tmp_schemas_for_accountid;

select distinct 
	case when schemaname like '%bing%' then 'Bing' else 'Adwords' end as src
	,cast(schemaname as varchar(128)) schema_name
	,cast(tablename as varchar(128)) table_name
	,cast(schemaname + '.' + tablename as varchar(255)) schema_table
into reporting_mart.tmp_schemas_for_accountid
from pg_tables
where 
	tablename in ('ad_group_performance_report','adgroup_performance_report')		
;


/*
	Process to populate the dim_schema_table_map
	create table reporting_mart.tmp_schema_table (sname varchar(128), tname varchar(128));
*/

-- Truncate and populate temp table with Schema/Table list
truncate table reporting_mart.tmp_schema_table
;
insert into reporting_mart.tmp_schema_table (sname, tname)
select cast(schemaname as varchar(128)) sname, cast(tablename as varchar(128)) as tname from pg_tables
;

-- Insert New Schemas into Schema/Table map
insert into reporting_mart.dim_schema_table_map (mcc)
select distinct 
	sname 
from reporting_mart.tmp_schema_table x
left join reporting_mart.dim_schema_table_map y on x.sname = y.mcc
where
	y.mcc is null
;

-- Update the Source field based on dim_accounts_map
update reporting_mart.dim_schema_table_map
	set
		source = y.source
from reporting_mart.dim_schema_table_map x
join (
		select
			mcc
			,source
		from reporting_mart.dim_accounts_map
		group by 1,2
		) y on x.mcc = y.mcc
where
	x.source is null
;

-- Update the table existence for each of the schemas in question
update reporting_mart.dim_schema_table_map
	set
		campaign_table_exists = y.campaign_table_exists
		,adgroup_table_exists = y.adgroup_table_exists
		,account_table_exists = y.account_table_exists
from reporting_mart.dim_schema_table_map x
join (
		select
			sname
			,sum(case when tname = 'campaign_performance_report' then 1 else 0 end) as campaign_table_exists
			,sum(case when tname in ('ad_group_performance_report','adgroup_performance_report') then 1 else 0 end) as adgroup_table_exists
			,sum(case when tname = 'accounts' then 1 else 0 end) as account_table_exists
		from reporting_mart.tmp_schema_table
		group by 1
		) y on x.mcc = y.sname
;
